(function ($) {
  /**
   * Common functions for the file chooser webform component.
   */

  Drupal.behaviors.fileUploads = {
    attach: function(context, settings) {
      var $chooser = $('.webform-component-filechooser');
      // Attach markup to create a UI file list.
      $chooser.prepend('<div class="webform-file-list"><ul></ul></div>');

      // Hide the textarea.
      $chooser.find('.form-textarea-wrapper').hide();
    }
  };
}(jQuery));

var $ = jQuery;

// Remove an uploaded File from a webform component.
function removeWebformFile(element) {
  // Re-enable the buttons.
  $('.webform-component-filechooser a.button').removeClass('ui-state-disabled').css('pointer-events', 'initial');

  var $link = $(element).closest('.uploaded-cv');

  var href = $link.find('.file-view-link').attr('href');
  var index = $link.find('.file-view-link').attr('index');

  // If we are dealing with a link that has an index (google drive), use the
  // index for removing the relevant file.
  if (index) {
    href = index;
  }

  // The item being removed is contained in a textarea ready for submission.
  var $textarea = $('.webform-component-filechooser textarea');
  var fileList = $textarea.val();

  // There are more than one file in the list.
  if (fileList.indexOf('|') != -1) {
    var files = fileList.split('|');

    // Keep the files that haven't been removed.
    var newFiles = [];
    $.each(files, function(key, fileUrl) {
      if (fileUrl.search(href) === -1) {
        newFiles.push(fileUrl);
      }
    });

    var text = '';
    // Check if more than one file remaining.
    if (newFiles.length > 1) {
      var length = newFiles.length;

      // Build the remaining file list.
      $.each(newFiles, function(key, file) {
        // Not the last item so add a delimeter.
        if (key != length - 1) {
          text += file;
          text += '|';
        }
        else {
          text += file;
        }
      });

      $textarea.val(text);
    }

    // Only one file remains so set it as the textarea value.
    else {
      $textarea.val(newFiles.toString());
    }
  }

  // No files remaining so set the textarea to empty.
  else {
    $textarea.val('');
  }

  // Remove the file link from the list.
  $link.remove();
}

// Remove surplus links/files from a dataset.
function removeSurplus(data, count, max, notify) {
  var newFiles = data.length;
  var total = newFiles + count;

  // If there is no limit on uploadable files OR we haven't exceeded the limit, don't remove any.
  if (total <= max || max < 0) {
    return data;
  }

  else {
    // How many files are we over the limit?
    var surplus = max - total;

    // Remove the surplus.
    data.splice(surplus);

    // As we remove surplus files from multiple locations, only notify where required.
    if (notify) {
      var message = Drupal.t("The maximum number of files is " + max + ", some files have been removed");
      alert(message);
    }

    return data;
  }
}

// Common function to build a list of files.
function buildFileList(_links, files) {
  var $textArea = $('.webform-component-filechooser textarea');

  var existingValue = $textArea.val();
  var countFiles = $('.webform-file-list .uploaded-file').length;

  // Get the max file upload limit.
  if (Drupal.settings.fileChooserWebformLimit) {
    var maxFiles = Drupal.settings.fileChooserWebformLimit;
  }

  // There are already some files in the list so merge them with the new files.
  if (existingValue.length) {
    var existingLinks = existingValue.split('|');
    _links = removeSurplus(_links, countFiles, maxFiles);

    _links = jQuery.merge(existingLinks, _links);
  }

  // Set the file text area with the complete file list.
  $textArea.val(_links.join('|'));

  // As the google API returns a different string for the file URL, we need
  // to identify this and add an attribue other than href that we can target
  // for file removal.
  var attr = '';

  files = removeSurplus(files, countFiles, maxFiles, true);

  var count = 0;
  $.each(files, function(key, file) {
    count++;

    if (file.url) {
      file.link = file.url;
      attr = file.id;
    }

    // Built the markup for the list item, along with controls to view and remove.
    var cvRow = '<li class="uploaded-cv pdf"><span class="uploaded-file"><a class="file-view-link" target="_blank" index="' + attr + '" href="' + file.link + '">' + file.name + '</a></span>\n\
      <span class="upload-component"><a class="remove-link webform-file" href="javascript:void(0);" onclick="removeWebformFile(this)">' + Drupal.t('Remove') + '</a>\n\
      <a class="view-link" target="_blank" href="' + file.link + '">' + Drupal.t('View File') + '</a></span></li>';

    // Add this file to the list.
    $('.webform-file-list').append(cvRow);

    // We have hit the limit of files, so disable the pickers.
    if (count >= maxFiles) {
      $('.webform-component-filechooser a.button').addClass('ui-state-disabled').css('pointer-events', 'none');
    }
  });
}
