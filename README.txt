
-- SUMMARY --
This module extends the File Chooser Field module with a webform component


-- FEATURES --
* Adds Dropbox, Google Drive and Box picker upload options to a webform.


-- INSTALLATION --
* Install this module.
* Create a webform with a ‘File chooser’ component
* Configure the component as required


-- CONTACT --

Current maintainers:
* aaron.ferris - https://www.drupal.org/u/aaron.ferris
