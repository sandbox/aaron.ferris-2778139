<?php

/**
 * @file
 * A webform component to capture all incoming URLs from the file chooser UI.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_filechooser() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'required' => 0,
    'extra' => array(
      'filtering' => array(
        'types' => array('txt pdf docx doc rtf'),
        'addextensions' => '',
        'size' => '2MB',
      ),
      'scheme' => 'private',
      'dropbox' => 0,
      'google_drive' => 0,
      'box' => 0,
      'one_drive' => 0,
      'max_files' => '',
      'private' => FALSE,
    ),
    'title_display' => 0,
    'attributes' => array(),
    'analysis' => FALSE,
  );
}

/**
 * Implementation of _webform_theme_component().
 */
function _webform_theme_filechooser() {
  return array(
    'file_chooser_field_webform_file' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_filechooser($component) {
  $form = array();
  $form['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t('The default value of the field.'),
    '#cols' => 60,
    '#rows' => 5,
    '#weight' => 0,
  );

  $form['extra']['size'] = array(
    '#type' => 'textfield',
    '#title' => t('Max upload size'),
    '#default_value' => $component['extra']['filtering']['size'],
    '#description' => t('Enter the max file size a user may upload such as 2 MB or 800 KB. Your server has a max upload size of @size.', array('@size' => format_size(file_upload_max_size()))),
    '#size' => 10,
    '#parents' => array('extra', 'filtering', 'size'),
    '#weight' => 1,
  );

  $form['extra']['max_files'] = array(
    '#type' => 'select',
    '#title' => t('Maximum number of files'),
    '#default_value' => $component['extra']['max_files'],
    '#options' => array(FIELD_CARDINALITY_UNLIMITED => t('Unlimited')) + drupal_map_assoc(range(1, 10)),
    '#description' => t('The maximum number of allowed files'),
    '#parents' => array('extra', 'max_files'),
    '#weight' => 2,
  );

  $form['extra']['extensions'] = array(
    '#parents' => array('extra', 'filtering'),
    '#theme_wrappers' => array('form_element'),
    '#default_value' => $component['extra']['filtering']['types'],
    '#title' => t('Separate extensions with a space or comma and do not include the leading dot.'),
    '#parents' => array('extra', 'filtering', 'types'),
    '#type' => 'textfield',
    '#weight' => 3,
  );

  $form['extra']['dropbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Dropbox'),
    '#default_value' => $component['extra']['dropbox'],
    '#description' => t(''),
    '#parents' => array('extra', 'dropbox'),
    '#weight' => 4,
  );

  $form['extra']['google_drive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Google Drive'),
    '#default_value' => $component['extra']['google_drive'],
    '#description' => t(''),
    '#parents' => array('extra', 'google_drive'),
    '#weight' => 5,
  );

  $form['extra']['box'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Box File Picker'),
    '#default_value' => $component['extra']['box'],
    '#description' => t(''),
    '#parents' => array('extra', 'box'),
    '#weight' => 6,
  );

  // $form['extra']['one_drive'] = array(
  //   '#type' => 'checkbox',
  //   '#title' => t('Enable One Drive'),
  //   '#default_value' => $component['extra']['one_drive'],
  //   '#description' => t(''),
  //   '#parents' => array('extra', 'one_drive'),
  //   '#weight' => 7,
  // );

  $scheme_options = array();
  foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
    $scheme_options[$scheme] = $stream_wrapper['name'];
  }

  $form['extra']['scheme'] = array(
    '#type' => 'radios',
    '#title' => t('Upload destination'),
    '#options' => $scheme_options,
    '#default_value' => $component['extra']['scheme'],
    '#description' => t('Private file storage has significantly more overhead than public files, but restricts file access to users who can view submissions.'),
    '#weight' => 8,
    '#access' => count($scheme_options) > 1,
  );

  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_filechooser($component, $value = NULL, $filter = TRUE) {
  $path = drupal_get_path('module', 'file_chooser_field_webform');
  drupal_add_js($path . '/js/file_chooser_field_webform.js');
  drupal_add_css($path . '/css/file_chooser_field_webform.css');

  $chooser = array();

  if (module_exists('file_chooser_field')) {
    module_load_include('inc', 'file_chooser_field');
    $path = drupal_get_path('module', 'file_chooser_field');

    $file_size = parse_size($component['extra']['filtering']['size']);
    $extensions = $component['extra']['filtering']['types'];
    $max_files = $component['extra']['max_files'];

    if (!empty($max_files)) {
      $js_setting = array(
        'fileChooserWebformLimit' =>  $max_files,
      );
      drupal_add_js($js_setting, 'setting');
    }

    $multiselect = true;
    if ($max_files === '1') {
      $multiselect = false;
    }

    $upload_validators = array();
    $upload_validators['file_validate_size'][] = $file_size;
    $upload_validators['file_validate_extensions'][] = $extensions;
    $description = theme('file_upload_help', array('description' => '', 'upload_validators' => $upload_validators));

    $info = array(
      'cardinality'       =>  $max_files,
      'description'       =>  $description,
      'upload_validators' =>  $upload_validators,
      'multiselect'       =>  $multiselect,
    );

    $plugins = file_chooser_field_load_plugins();
    foreach ($plugins as $name => $plugin) {
      // Don't add this chooser if the individual plugin isn't enabled in the
      // component.
      if (!empty($component['extra'][$name])) {
        $status = file_chooser_field_plugin_method($plugin['phpClassName'], 'getStatus');
        if ($status && user_access('upload files from ' . $name)) {
          // Button attributes.
          $attributes = file_chooser_field_plugin_method($plugin['phpClassName'], 'attributes', array($info));
          // Button label.
          $label = file_chooser_field_plugin_method($plugin['phpClassName'], 'label');
          // Button CSS class.
          $cssClass = file_chooser_field_plugin_method($plugin['phpClassName'], 'cssClass');
          // Load all requried assets.
          file_chooser_field_plugin_method($plugin['phpClassName'], 'assets');
          $chooser[] = theme('file_chooser_field', array(
              'label' => $label,
              'class' => $cssClass,
              'attributes' => $attributes,
            )
          );
        }
      }
    }
  }

  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $prefix = '';
  foreach($chooser as $markup) {
    $prefix .= $markup;
  }

  $element = array(
    '#type' => 'textarea',
    '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#default_value' => '',
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#field_prefix' => $prefix,
    '#theme_wrappers' => array('webform_element'),
    '#translatable' => array('title', 'description'),
  );

  if (isset($value[0])) {
    $element['#default_value'] = $value[0];
  }

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_filechooser($component, $value, $format = 'html') {
  $fids = isset($value[0]) ? unserialize($value[0]) : NULL;

  return array(
    '#title' => $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#weight' => $component['weight'],
    '#theme' => 'file_chooser_field_webform_file',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#value' => $fids ? file_chooser_field_webform_get_files($fids) : '',
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_filechooser($component, $sids = array()) {
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid']);

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $nonblanks = 0;
  $submissions = 0;
  $wordcount = 0;

  $result = $query->execute();
  foreach ($result as $data) {
    if (drupal_strlen(trim($data['data'])) > 0) {
      $nonblanks++;
      $wordcount += str_word_count(trim($data['data']));
    }
    $submissions++;
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('User entered value'), $nonblanks);

  $other[] = array(t('Average submission length in words (ex blanks)'), ($nonblanks != 0 ? number_format($wordcount/$nonblanks, 2) : '0'));

  return array(
    'table_rows' => $rows,
    'other_data' => $other,
  );
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_filechooser($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_filechooser($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_filechooser($component, $export_options, $value) {
  return !isset($value[0]) ? '' : $value[0];
}

/**
 * Implementation of _webform_submit_component().
 */
function _webform_submit_filechooser($component, $value) {
  if (!empty($value)) {
    $file_urls = explode('|', $value);

    $file_size = parse_size($component['extra']['filtering']['size']);
    $extensions = $component['extra']['filtering']['types'];

    $element = array();
    $element['#upload_location'] = 'private://';
    $element['#file_chooser_field_upload_validators']['file_validate_extensions'][] = $extensions;
    $element['#file_chooser_field_upload_validators']['file_validate_size'][] = $file_size;

    $element['#parents'] = array('webform-component-filechooser');

    module_load_include('inc', 'file_chooser_field', 'file_chooser_field.field');

    $fids = array();
    foreach ($file_urls as $key => $file) {
      $file = file_chooser_field_save_upload($element, $file);
      $fids[] = $file->fid;
    }

    return serialize($fids);
  }
}

/**
 * Helper function to load a file from the database.
 */
function file_chooser_field_webform_get_files($fids) {
  static $files;
  $allfiles = array();
  if (empty($fids)) return array();
  foreach ($fids as $fid) {
    if (!isset($files[$fid])) {
      if (empty($fid)) {
        $files[$fid] = FALSE;
      }
      else {
        $files[$fid] = file_load($fid);
      }
    }
    $allfiles[$fid] = $files[$fid];
  }

  return $allfiles;
}

/**
 * Format the output of data for this component.
 */
function theme_file_chooser_field_webform_file($variables) {
  module_load_include('inc', 'webform', 'components/file');
  $element = $variables['element'];
  $output = '';

  if (!empty($element['#value'])) {
    $files = $element['#value'];

    foreach ($files as $file) {
      if (!empty($file)) {
        $url = webform_file_url($file->uri);
        $filename = urldecode($file->filename);

        if ($element['#format'] == 'html') {
          $output .= '<div class="multifile-file"> ';
          $output .= l($filename, $url);
          $output .= ' </div>';
        }
        else {
          $output .= $filename . ': ' . $url . "\n";
        }
      }
    }
  }

  return $output;
}
